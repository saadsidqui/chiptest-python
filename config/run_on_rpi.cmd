@echo off
del /Q ChipTest.zip >nul 2>&1
ssh pi@192.168.1.31 "rm -f ~/Desktop/ChipTest.zip && mkdir -p ~/Desktop/ChipTest && cd ~/Desktop/ChipTest && find . -mindepth 1 -name virtualenv -prune -o -exec rm -rf {} +"
tar -c -z --exclude ./virtualenv/* --exclude ./.git/ --exclude ./config/* --exclude ./ChipTest.zip -f ChipTest.zip .
scp ./ChipTest.zip pi@192.168.1.31:~/Desktop/ChipTest
ssh pi@192.168.1.31 "cd ~/Desktop/ChipTest && tar -xf ChipTest.zip && rm -rf ./ChipTest.zip"
del /Q ChipTest.zip
ssh pi@192.168.1.31 "cd ~/Desktop/ChipTest && source ./virtualenv/bin/activate && python3 main.py"
del /Q output.txt >nul 2>&1
scp pi@192.168.1.31:~/Desktop/ChipTest/output.txt ./