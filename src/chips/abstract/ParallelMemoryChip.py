import json
import random
import time
from abc import ABC

from src.chips.abstract.MemoryChip import MemoryChip
from src.utils.Utils import progress


class ParallelMemoryChip(MemoryChip, ABC):
	def __init__(self, logger):
		super().__init__()
		self.log = logger

	def __mscan(self):
		result = True

		total_iter = (self.size * 2) - 1
		start_ns = time.perf_counter_ns()
		r = -1
		self.log.info(self.__class__.__name__ + ": Starting MSCAN (up)")
		for i in range(self.size):
			for v in [0, self.data_max]:
				self.write(i, v)
				r = self.read(i)
				if r != v:
					result = False
					self.log.error("address=" + str(i) + " read " + str(r) + ", expected " + str(v))
				progress(i, total_iter, 'MSCAN (up): ' + str(i) + "  ")

		self.log.info(self.__class__.__name__ + ": Starting MSCAN (down)")
		for i in reversed(range(self.size)):
			for v in [0, self.data_max]:
				self.write(i, v)
				r = self.read(i)
				if r != v:
					result = False
					self.log.error("address=" + str(i) + " read " + str(r) + ", expected " + str(v))
				progress(total_iter - i, total_iter, 'MSCAN (down): ' + str(i) + "  ")
		end_ns = time.perf_counter_ns()
		diff_ns = end_ns - start_ns
		diff_s = diff_ns / (10 ** 9)
		avg = diff_ns / (total_iter + 1)
		stats = "{:s}: MSCAN done in {:.2f} sec, average ~{:.2f} ns".format(self.__class__.__name__, diff_s, avg)
		self.log.info(stats)
		self.log.flush()
		print("")
		print(stats)
		return result

	def __march(self):
		result = True

		total_iter = (self.size * 6) - 1
		start_ns = time.perf_counter_ns()
		counter = 0
		r = -1

		self.log.info(self.__class__.__name__ + ": Starting MARCH (up, 1), [write 0s]")
		for i in range(self.size):
			self.write(i, 0)
			progress(counter, total_iter, 'MARCH (up, 1): ' + str(i) + "  ")
			counter += 1

		self.log.info(self.__class__.__name__ + ": Starting MARCH (up, 2), [read 0s, write 1s]")
		for i in range(self.size):
			r = self.read(i)
			if r != 0:
				result = False
				self.log.error("address=" + str(i) + " read " + str(r) + ", expected 0")
			self.write(i, self.data_max)
			progress(counter, total_iter, 'MARCH (up, 2): ' + str(i) + "  ")
			counter += 1

		self.log.info(self.__class__.__name__ + ": Starting MARCH (up, 3), [read 1s, write 0s]")
		for i in range(self.size):
			r = self.read(i)
			if r != self.data_max:
				result = False
				self.log.error("address=" + str(i) + " read " + str(r) + ", expected " + self.data_max_str)
			self.write(i, 0)
			progress(counter, total_iter, 'MARCH (up, 3): ' + str(i) + "  ")
			counter += 1

		self.log.info(self.__class__.__name__ + ": Starting MARCH (down, 1), [read 0s, write 1s]")
		for i in reversed(range(self.size)):
			r = self.read(i)
			if r != 0:
				result = False
				self.log.error("address=" + str(i) + " read " + str(r) + ", expected 0")
			self.write(i, self.data_max)
			progress(counter, total_iter, 'MARCH (down, 1): ' + str(i) + "  ")
			counter += 1

		self.log.info(self.__class__.__name__ + ": Starting MARCH (down, 2), [read 1s, write 0s]")
		for i in reversed(range(self.size)):
			r = self.read(i)
			if r != self.data_max:
				result = False
				self.log.error("address=" + str(i) + " read " + str(r) + ", expected " + self.data_max_str)
			self.write(i, 0)
			progress(counter, total_iter, 'MARCH (down, 2): ' + str(i) + "  ")
			counter += 1

		self.log.info(self.__class__.__name__ + ": Starting MARCH (down, 2), [read 0s]")
		for i in reversed(range(self.size)):
			r = self.read(i)
			if r != 0:
				result = False
				self.log.error("address=" + str(i) + " read " + str(r) + ", expected 0")
			progress(counter, total_iter, 'MARCH (down, 3): ' + str(i) + "  ")
			counter += 1

		end_ns = time.perf_counter_ns()
		diff_ns = end_ns - start_ns
		diff_s = diff_ns / (10 ** 9)
		avg = diff_ns / (total_iter + 1)
		stats = "{:s}: MARCH done in {:.2f} sec, average ~{:.2f} ns".format(self.__class__.__name__, diff_s, avg)
		self.log.info(stats)
		self.log.flush()
		print("")
		print(stats)
		return result

	def __checkerboard(self):
		result = True

		total_iter = (self.size * 4) - 1
		counter = 0
		start_ns = time.perf_counter_ns()
		r = -1
		cur_bit = 0

		for start_bit in [self.data_max, 0]:
			start_bit_str = str(self.data_max if start_bit == 0 else 0)
			self.log.info(self.__class__.__name__ + ": Starting CHECKERBOARD (" + start_bit_str + " start)")

			cur_bit = start_bit
			for i in range(self.size):
				cur_bit = self.data_max if cur_bit == 0 else 0
				self.write(i, cur_bit)
				counter += 1
				progress(counter, total_iter, "CHECKERBOARD (" + start_bit_str + " start) " + str(i) + "  ")

			cur_bit = start_bit
			for i in range(self.size):
				cur_bit = self.data_max if cur_bit == 0 else 0
				r = self.read(i)
				if r != cur_bit:
					result = False
					self.log.error("address=" + str(i) + " read " + str(r) + ", expected " + str(cur_bit))
				counter += 1
				progress(counter, total_iter, "CHECKERBOARD (" + start_bit_str + " start) " + str(i) + "  ")

		end_ns = time.perf_counter_ns()
		diff_ns = end_ns - start_ns
		diff_s = diff_ns / (10 ** 9)
		avg = diff_ns / (total_iter + 1)
		stats = "{:s}: CHECKERBOARD done in {:.2f} sec, average ~{:.2f} ns".format(self.__class__.__name__, diff_s, avg)
		self.log.info(stats)
		self.log.flush()
		print("")
		print(stats)
		return result

	def __data_retention(self, delay):
		result = True

		total_iter = (self.size * 2) - 1
		start_ns = time.perf_counter_ns()
		counter = 0
		r = -1

		data = [round(random.uniform(0, self.data_max)) for _ in range(self.size)]

		self.log.info(self.__class__.__name__ + ": Starting DATA RETENTION FOR " + str(delay) + " SECONDS (write)")
		for i in range(self.size):
			self.write(i, data[i])
			progress(counter, total_iter, 'DATA RETENTION (write): ' + str(i) + "  ")
			counter += 1

		progress(counter, total_iter, 'DATA RETENTION (sleep ' + str(delay) + "s)")
		time.sleep(delay)

		self.log.info(self.__class__.__name__ + ": Starting DATA RETENTION FOR " + str(delay) + " SECONDS (read)")
		for i in range(self.size):
			r = self.read(i)
			if r != data[i]:
				result = False
				self.log.error("address=" + str(i) + " read " + str(r) + ", expected " + str(data[i]))
			progress(counter, total_iter, 'DATA RETENTION (read): ' + str(i) + "  ")
			counter += 1

		end_ns = time.perf_counter_ns()
		diff_ns = end_ns - start_ns
		diff_s = diff_ns / (10 ** 9)
		avg = diff_ns / (total_iter + 1)
		stats = "{:s}: DATA RETENTION FOR {:d} SECONDS done in {:.2f} sec, average ~{:.2f} ns".format(
			self.__class__.__name__, delay, diff_s, avg
		)
		self.log.info(stats)
		self.log.flush()
		print("")
		print(stats)
		return result
		pass

	def test(self):
		result = {
			"pass": True,
			"algorithms": {
				"mscan": False,
				"march": False,
				"checkerboard": False,
			}
		}
		# MSCAN is not very effective. Skipped.
		# result["algorithms"]["mscan"] = self.__mscan()

		r = self.__checkerboard()
		result["algorithms"]["checkerboard"] = r

		r = self.__march()
		result["algorithms"]["march"] = r

		r = self.__data_retention(120)
		result["algorithms"]["data_retention_120"] = r

		for i in result["algorithms"]:
			if not result["algorithms"][i]:
				result["pass"] = False
				break

		self.log.info(json.dumps(result))
		self.log.info(self.__class__.__name__ + ": Testing done.")
		return result
