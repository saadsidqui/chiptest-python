from abc import ABC, abstractmethod
import os

class MemoryChip(ABC):
	def __init__(self):
		super().__init__()
		self.size = 1
		self.word_size = 1
		self.data_max = 1
		self.data_max_str = 1

	@abstractmethod
	def setup(self):
		pass

	@abstractmethod
	def read(self, addr):
		pass

	@abstractmethod
	def write(self, addr, data):
		pass

	def read_to_file(self, filename):
		file = None
		try:
			file = open(filename, 'wb')
			for i in range(self.size):
				file.write(self.read(i))
		except BaseException as e:
			print(e)
		finally:
			if file is not None:
				file.close()

	def write_from_file(self, filename, offset=0):
		file = None
		try:
			file = open(filename, 'rb')
			size = max(self.size, os.path.getsize(filename))
			if (offset + size) > self.size:
				size = self.size - offset
			for i in range(size):
				self.write(i + offset, file.read())
		except BaseException as e:
			print(e)
		finally:
			if file is not None:
				file.close()
