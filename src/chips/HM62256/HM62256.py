import RPi.GPIO as GPIO
import time
from src.chips.abstract.ParallelMemoryChip import ParallelMemoryChip
from src.utils.Utils import bit_bang, read_pins, progress


class HM62256(ParallelMemoryChip):
	def __init__(self, logger):
		super().__init__(logger)
		self.size = 32768
		self.word_size = 8
		self.log = logger
		self.data_max = ((2 ** self.word_size) - 1)
		self.data_max_str = str(self.data_max)

		self.A = range(4, 19)       # A0 (4) - A14 (18)
		self.D = range(19, 27)      # D0 (19) - D7 (26)
		self.cs_pin = 2             # Chip select
		self.we_pin = 3             # Write enable
		self.oe_pin = 27            # Output enable

		self.taa_wait = 150e-9      # Address access time 150ns
		self.tohz_wait = 50e-9      # Output disable to output in high-Z 50ns

		self.tas_wait = 0           # Address setup 0ns
		self.tow_wait = 5e-9      	# Output active from end of write 5ns
		self.tcw_wait = 150e-9  	# Min write cycle 150ns

	def __del__(self):
		GPIO.cleanup()

	def setup(self):
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self.cs_pin, GPIO.OUT)
		GPIO.output(self.cs_pin, GPIO.HIGH)

		GPIO.setup(self.we_pin, GPIO.OUT)
		GPIO.output(self.we_pin, GPIO.HIGH)

		GPIO.setup(self.oe_pin, GPIO.OUT)
		GPIO.output(self.oe_pin, GPIO.HIGH)

		for pin in self.A:
			GPIO.setup(pin, GPIO.OUT)

	def read(self, addr):
		addr = int(addr)
		if addr >= self.size:
			raise ValueError('Address is out of bounds for this chip')

		for d in self.D:
			GPIO.setup(d, GPIO.IN)

		bit_bang(addr, self.A)

		GPIO.output(self.cs_pin, GPIO.LOW)
		GPIO.output(self.oe_pin, GPIO.LOW)
		time.sleep(self.taa_wait)
		value = read_pins(self.D)
		GPIO.output(self.oe_pin, GPIO.HIGH)
		GPIO.output(self.cs_pin, GPIO.HIGH)
		time.sleep(self.tohz_wait)
		return value

	def write(self, addr, data):
		addr = int(addr)
		data = int(data)

		if addr >= self.size:
			raise ValueError('Address is out of bounds for this chip')

		if (data < 0) or (data > self.data_max):
			raise ValueError('Value is out of bounds for this chip')

		for d in self.D:
			GPIO.setup(d, GPIO.OUT)

		GPIO.output(self.oe_pin, GPIO.HIGH)
		time.sleep(self.tohz_wait)

		bit_bang(addr, self.A)
		time.sleep(self.tas_wait)

		for i, d in enumerate(self.D):
			bit = GPIO.HIGH if bool(data & 1 << i) else GPIO.LOW
			GPIO.output(d, bit)

		GPIO.output(self.cs_pin, GPIO.LOW)
		GPIO.output(self.we_pin, GPIO.LOW)
		time.sleep(self.tcw_wait)

		GPIO.output(self.we_pin, GPIO.HIGH)
		GPIO.output(self.cs_pin, GPIO.HIGH)
		time.sleep(self.tow_wait)
