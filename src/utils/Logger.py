from datetime import datetime


class Logger:
	__handle = None

	def __init__(self, filename):
		self.log_date = False
		self.log_time = True
		self.__handle = open(filename, 'w')

	def __del__(self):
		self.close()

	def getDateTimeString(self):
		now = datetime.now()
		result = []
		if self.log_date:
			result.append(now.strftime("%d-%b-%Y"))
		if self.log_time:
			result.append(now.strftime("%H:%M:%S"))
		return " ".join(result)

	def log(self, msg):
		self.__handle.write(self.getDateTimeString() + " " + msg)

	def line(self, msg):
		self.log(msg + "\n")

	def error(self, msg):
		self.line("[X] " + msg)

	def warning(self, msg):
		self.line("/!\\ " + msg)

	def info(self, msg):
		self.line("[i] " + msg)

	def newline(self):
		self.__handle.write("\n")

	def flush(self):
		self.__handle.flush()

	def close(self):
		try:
			if self.__handle and not self.__handle.closed:
				self.flush()
				self.__handle.close()
		except BaseException as e:
			print(e)
