import RPi.GPIO as GPIO
import sys


def progress(count, total, status=''):
	bar_len = 60
	filled_len = int(round(bar_len * count / float(total)))

	percents = round(100.0 * count / float(total), 1)
	bar = '=' * filled_len + '-' * (bar_len - filled_len)

	sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
	sys.stdout.flush()


def bit_bang(value, pins):
	if (not isinstance(pins, range)) or (len(pins) < 1):
		raise ValueError('Invalid pins')

	if (not isinstance(value, int)) or (value > ((2 ** len(pins)) - 1)):
		raise ValueError('Invalid value')

	for i, p in enumerate(pins):
		bit = GPIO.HIGH if bool(value & 1 << i) else GPIO.LOW
		GPIO.output(p, bit)


def read_pins(pins):
	if (not isinstance(pins, range)) or (len(pins) < 1):
		raise ValueError('Invalid pins')

	value = 0
	for i, p in enumerate(pins):
		value += GPIO.input(p) * (2 ** i)

	return value
