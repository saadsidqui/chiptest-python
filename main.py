from src.chips.HM62256.HM62256 import HM62256
from src.utils.Logger import Logger
from datetime import datetime
import RPi.GPIO as GPIO
import time

if __name__ == '__main__':
    logger = None
    try:
        GPIO.setwarnings(False)
        GPIO.cleanup()

        log_file = "output.txt"  # + datetime.now().strftime("%d%b%Y_%H%M%S%f") + ".txt"
        logger = Logger(log_file)
        chip = HM62256(logger)
        chip.setup()

        start = time.perf_counter()
        pass_count = 0
        iterations = 3
        for i in range(iterations):
            pass_str = "Iteration " + str((i + 1))
            print(pass_str)
            logger.info(pass_str)
            result = chip.test()
            if result["pass"]:
                pass_count += 1
            print(result)

        end = time.perf_counter()
        diff = end - start
        msg = "Passed " + str(pass_count) + " of " + str(iterations) + "iterations in " + str(diff) + " seconds."
        logger.info(msg)
        print(msg)

    except BaseException as e:
        print(e)
    finally:
        if logger:
            logger.close()
        GPIO.setwarnings(False)
        GPIO.cleanup()
